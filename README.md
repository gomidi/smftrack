# smftrack

**Please note:** smftrack moved to `gitlab.com/gomidi/midi/tools/smftrack`. All further development will be done in the `gitlab.com/gomidi/midi` repo.
